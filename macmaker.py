#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 07:40:50 2022
Makes mac files
"""

# import module
import numpy as np
import os

  
# Define Constants
len_unit = "mm"
nrg_unit = "eV"


class macmaker():
    def __init__(self, code):
        self.macfile = "charge_hits_{}.mac".format(code)
        self.outputlocation = "output/"
        self.hitfile = "charge_hits_{}.txt".format(code)
        self.locmode = "fixed"
        self.dirmode = "fixed"
        self.locs = [(0,0,0)]
        self.dirs = [(0,0,1)]
        self.nrg = 0.78
        
        self.num_loc = len(self.locs)
        self.num_dir = len(self.dirs)
        
        
        self.xrange, self.yrange, self.zrange, self.buffer = [-1,1], [-1,1], [-0.3,0.3], [0.01,0.01,0.01]
        self.__generic_commands()
        
    def set_nrg(self, nrg):
        '''
        

        Parameters
        ----------
        nrg : float
            Set the energy of the gun
        '''
        self.nrg = nrg
        
    def randomize_loc(self, num_loc):
        '''
        Creates list of n_loc random locations for list of location

        '''
        self.locmode = "random"
        self.num_loc = num_loc
        
        self.locs = []
        for _ in range(self.num_loc):
            self.locs.append(self.gen_random_pos(self.xrange, self.yrange, self.zrange, self.buffer))
        
    def fix_loc(self, locs):
        '''
        Set the list of locations
        Parameters
        ----------
        dirs : list of 3-ary tuples 
        '''
        self.locmode = "fixed"
        self.locs = locs
        
    def randomize_dir(self, num_dir):
        '''
        Creates n_dir random unit vector for list of direction

        '''
        self.dirmode = "random"
        self.num_dir = num_dir
        
        self.dirs = []
        for _ in range(self.num_dir):
            self.dirs.append(self.gen_random_direction())
            
        
    def fix_dir(self, dirs):
        '''
        Set the list of directions
        Parameters
        ----------
        dirs : list of 3-ary tuples 

        '''
        self.dirmode = "fixed"
        self.dirs = dirs
        
    def write_random(self, number):
        '''
        Writes given number of random location and direction (independent)

        Parameters
        ----------
        number : Integer
            Number of random location and direction

        '''
        with open(self.macfile, "a") as file:
            for _ in range(number):
                x, y, z = self.gen_random_pos(self.xrange, self.yrange, self.zrange, self.buffer)
                file.write("/gun/position {} {} {} {:s}\n".format(x,y,z, len_unit))
                x,y,z = self.gen_random_direction()
                file.write("/gun/direction {} {} {}\n".format(x,y,z))
                file.write("/run/beamOn 1\n\n")
    
    def write(self):
        """
        Writes the files with configuration of direction and position.
        Itreates through list of store location and list of direction 

        """
        with open(self.macfile, "a") as file:
            for loc in self.locs:
                x, y, z = loc
                file.write("/gun/position {} {} {} {:s}\n".format(x,y,z, len_unit))
                for direction in self.dirs:
                    x,y,z = direction
                    file.write("/gun/direction {} {} {}\n".format(x,y,z))
                    file.write("/run/beamOn 1\n\n")
    
    def __generic_commands(self):
        """
        Writes preamble

        """
        if os.path.exists(self.macfile):
            os.remove(self.macfile)
        
        with open(self.macfile, "a") as file:
            file.write("/run/initialize\n/gun/number 1\n\n/g4cmp/phononBounces 10000000\n/tracking/verbose 0\n")
            file.write("/g4cmp/HitsFile {:s}{:s}\n\n".format(self.outputlocation,self.hitfile))
            file.write("# Gun Randomization\n")
            file.write("/gun/energy {:s} {:s}\n".format(str(self.nrg),nrg_unit))
        
    def gen_random_pos(self,xrange, yrange, zrange, buffer):
        """
        Generates a random location within the range, and buffer
        Return: 3-ary tuple, 3d position vector
        """
        x = np.random.uniform(xrange[0] + buffer[0], xrange[1] - buffer[0])
        y = np.random.uniform(yrange[0] + buffer[1], yrange[1] - buffer[1])
        z = np.random.uniform(zrange[0] + buffer[2], zrange[1] - buffer[2])
        return (x, y, z)
    
    
    def gen_random_direction(self):
        """
        Generates a random 3D unit vector (direction) with a uniform spherical distribution
        Return: 3-ary tuple, 3d unit vector
        """
        phi = np.random.uniform(0,np.pi*2)
        costheta = np.random.uniform(-1,1)
    
        theta = np.arccos( costheta )
        x = np.sin( theta) * np.cos( phi )
        y = np.sin( theta) * np.sin( phi )
        z = np.cos( theta )
        return (x, y, z)


    

if __name__ == "__main__":
    objMM = macmaker('random_1000')
    objMM.write_random(1000)
