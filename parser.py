"""
Created on Wed Jul  6 12:22:25 2022
A preliminary data analysis script
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import seaborn as sns
import pickle


def get_xyz(dbiQ):
    fx = np.array(dbiQ['End X [mm]'], dtype = float)
    fy = np.array(dbiQ['End Y [mm]'], dtype = float)
    fz = np.array(dbiQ['End Z [mm]'], dtype = float)
    return fx, fy, fz

def get_ftime(dbiQ):
    return np.array(dbiQ['Final Time [us]'], dtype=float)

def get_stime(dbiQ):
    return np.array(dbiQ['Start Time [us]'], dtype=float)

def get_enrg(dbiQ):
    return np.array(dbiQ['Energy Deposited [eV]'], dtype=float)

def show_over_time(objP):
    '''
    Show Phonon Collection over time

    '''
    fig, ax = plt.subplots()
    bin_w = 0.1
    bin_n = np.arange(0, 20 + bin_w, bin_w)
    factor = 1/bin_w
    
    ftL  = get_ftime(objP.dbL)
    ftTS = get_ftime(objP.dbTS)
    ftTF = get_ftime(objP.dbTF)
    ftP  = get_ftime(objP.dbP)
    
    ax.hist([ftL, ftTS, ftTF], stacked = True, label = ["Longitudinal", "Tranverse Slow", "Transverse Fast"], bins = bin_n, weights = [factor * np.ones_like(ftL), factor * np.ones_like(ftTS), factor * np.ones_like(ftTF)])
    ax.hist(ftP, bins = bin_n, label = ["Total"], color = 'black', histtype= 'step', weights = factor * np.ones_like(ftP))
    
    ax.legend()
    ax.set_xlim(0,20)
    ax.ticklabel_format(axis = 'y', style = 'sci', scilimits = (0,1))
    plt.xlabel("Time [$\mu s$]")
    plt.ylabel("Rate of phonon collection [$\mu s^{-1}$]")
    plt.title("Phonon Collection over Time")
    
    return fig, ax

def show_over_nrg(objP):
    '''
    Show histogram of final energy of photon
    '''
    fig, ax = plt.subplots()
    bin_w = 0.1
    bin_n = np.arange(0, 30 + bin_w, bin_w)
    
    ftL  = get_enrg(objP.dbL)*1000
    ftTS = get_enrg(objP.dbTS)*1000
    ftTF = get_enrg(objP.dbTF)*1000
    ftP  = get_enrg(objP.dbP)*1000
    
    ax.hist([ftL, ftTS, ftTF], stacked = True, label = ["Longitudinal", "Tranverse Slow", "Transverse Fast"], bins = bin_n, density = True)
    ax.hist(ftP, bins = bin_n, label = ["Total"], color = 'black', histtype= 'step', density = True)
    
    ax.legend()
    ax.set_xlim(0,30)
    plt.xlabel("Final energy [meV]")
    plt.ylabel("Density")
    plt.title("Energy Distribution of collected phonon")
    
    return fig, ax

def show_over_space(dbiQ):
    '''
    2D histogram of where phonon lands
    '''
    fx, fy, _ = get_xyz(dbiQ)
    
    fig, ax = plt.subplots()
    hist = plt.hist2d(fx, fy, density=True, bins = 100, cmap = plt.cm.GnBu)
    plt.xlabel("X [mm]")
    plt.ylabel("Y[mm]")
    plt.title("Phonon Hit Profile")
    fig.colorbar(hist[3], ax=ax, )
    return hist

class parsed_hit():
    '''
    Packages hit list as database
    '''
    def __init__(self, filename):
        self.file = filename
        self.database = pd.read_csv(self.file)
        
        
        ## Change Units
        self.database['End X [m]'] = self.database['End X [m]']*1000
        self.database['End Y [m]'] = self.database['End Y [m]']*1000
        self.database['End Z [m]'] = self.database['End Z [m]']*1000
        self.database['Final Time [ns]'] = self.database['Final Time [ns]']/1000
        self.database['Start Time [ns]'] = self.database['Start Time [ns]']/1000
        self.database = self.database.rename(columns={"End X [m]": "End X [mm]", "End Y [m]": "End Y [mm]", "End Z [m]":"End Z [mm]", "Final Time [ns]":"Final Time [us]","Start Time [ns]":"Start Time [us]" })
        
    
        ## Create Partition Database
        self.dbTS = self.particle_filter('phononTS')
        self.dbTF = self.particle_filter('phononTF')
        self.dbL = self.particle_filter('phononL')
        self.dbP = self.particle_filter('phonon')
        self.dbEH = self.particle_filter('Drift')


    def particle_filter(self,particle):
        return self.database[self.database['Particle Name'].str.contains(particle)]
        
    
if __name__ == '__main__':
    pass
    # obj = parsed_hit("output/charge_hits_random_1000.txt")
