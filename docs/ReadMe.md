# Readme

## Requirements

CLHEP

Geant 4.10.5 patch 1

G4CMP V8

### Running the project on `centos7.slac.stanford.edu`'

Source these files before building or running the binary files.

```bash
source /afs/slac.stanford.edu/package/geant4/vol56/geant4.10.05.p01/rhel6-64/share/Geant4-10.5.1/geant4make/geant4make.sh
source /afs/slac.stanford.edu/package/geant4/vol56/geant4.10.05.p01/rhel6-64/bin/geant4.sh
source /afs/slac/g/cdms/software/G4CMP/g4cmp-V08-00-02/share/G4CMP/g4cmp_env.sh
```

## Creating macro

`[macmaker.py](http://macmaker.py)` can be modified to write create a macro file

**Example: Creating 1000 random position and momentum**

```python
objMM = macmaker('random_1000') # create a macmaker object with a filecode
objMM.write_random(1000) # Create 1000 runs of completety random position and momentun
```

**Example: Creating simulation for 1000 random direction for 3 given position**

```python
objMM = macmaker('pos_3_dir_1000')
objMM.fix_loc([(0,0,0), (1,0,0),(0,1,0)])
objMM.randomize_dir(1000)
objMM.write()
```

> For each of those given position, same set of 1000 random direction is used.
> 

**Example: Creating simulation for 100 random direction for 100 random position**

```python
objMM = macmaker('pos_3_dir_1000')
objMM.randomize_loc(100)
objMM.randomize_dir(100)
objMM.write()
```

> For each of those 100 positions, same set of 100 random direction is used.
> 

## Submitting job in `centos7.slac.stanford.edu`

`bsub -q <queue> <command>`

For example:

`bsub -q medium ./g4cmpCharge ../macros/charge_hit_000.mac`

For further documentation:

[Using the SLAC Batch Farm](https://www.slac.stanford.edu/exp/glast/wb/prod/pages/installingOfflineSW/usingSlacBatchFarm.htm)